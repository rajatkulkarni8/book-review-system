from fastapi import FastAPI
from Code.routers import books, reviews
from Code.database import engine, Base

# Create database tables
Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(books.router)
app.include_router(reviews.router)

@app.get("/")
def read_root():
    return {"message": "Welcome to the Book Review API!"}
