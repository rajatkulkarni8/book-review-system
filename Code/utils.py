from fastapi import BackgroundTasks

def send_review_email(email: str, message: str):
    # Simulate sending email (e.g., print to console)
    print(f"Sending email to {email}: {message}")

def background_send_review_email(background_tasks: BackgroundTasks, email: str, message: str):
    background_tasks.add_task(send_review_email, email, message)
