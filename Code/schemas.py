from pydantic import BaseModel
from typing import List, Optional

class BookBase(BaseModel):
    title: str
    author: str
    publication_year: int

class BookCreate(BookBase):
    pass

class Book(BookBase):
    id: int
    reviews: List['Review'] = []

    class Config:
        orm_mode = True

class ReviewBase(BaseModel):
    text: str
    rating: int

class ReviewCreate(ReviewBase):
    book_id: int

class Review(ReviewBase):
    id: int
    book: Book

    class Config:
        orm_mode = True
