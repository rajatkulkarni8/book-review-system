from fastapi.testclient import TestClient
from Code.main import app

client = TestClient(app)

def test_create_review():
    response = client.post("/reviews/", json={"text": "Great book!", "rating": 5, "book_id": 1})
    assert response.status_code == 200
    assert response.json()["text"] == "Great book!"

def test_read_reviews():
    response = client.get("/reviews/book/1")
    assert response.status_code == 200
    assert isinstance(response.json(), list)
