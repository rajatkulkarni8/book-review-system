from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from Code import crud, models, schemas
from Code.database import SessionLocal
from typing import List, Optional

router = APIRouter(
    prefix="/reviews",
    tags=["reviews"]
)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.post("/", response_model=schemas.Review)
def create_review(review: schemas.ReviewCreate, db: Session = Depends(get_db)):
    return crud.create_review(db=db, review=review)

@router.get("/book/{book_id}", response_model=List[schemas.Review])
def read_reviews(book_id: int, skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    reviews = crud.get_reviews(db, book_id=book_id, skip=skip, limit=limit)
    return reviews
