from sqlalchemy.orm import Session
from Code import models, schemas

def get_book(db: Session, book_id: int):
    return db.query(models.Book).filter(models.Book.id == book_id).first()

def get_books(db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.Book).offset(skip).limit(limit).all()

def create_book(db: Session, book: schemas.BookCreate):
    db_book = models.Book(**book.dict())
    db.add(db_book)
    db.commit()
    db.refresh(db_book)
    return db_book

def get_reviews(db: Session, book_id: int, skip: int = 0, limit: int = 10):
    return db.query(models.Review).filter(models.Review.book_id == book_id).offset(skip).limit(limit).all()

def create_review(db: Session, review: schemas.ReviewCreate):
    db_review = models.Review(text=review.text, rating=review.rating, book_id=review.book_id)
    db.add(db_review)
    db.commit()
    db.refresh(db_review)
    return db_review
